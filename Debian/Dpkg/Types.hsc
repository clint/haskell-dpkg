{-
 Types.hsc: Haskell bindings to libdpkg
   Copyright (C) 2011-2012 Clint Adams

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
-}

{-# LANGUAGE CPP, ForeignFunctionInterface #-}

#include <bindings.dsl.h>

module Debian.Dpkg.Types where
#strict_import

import Data.ByteString as BS

#include <dpkg/dpkg-db.h>
#include <dpkg/error.h>

#starttype struct dpkg_version
#field epoch , CInt
#field version , CString
#field revision , CString
#stoptype

#starttype struct filedetails
#field next , Ptr <filedetails>
#field name , CString
#field msdosname , CString
#field size , CString
#field md5sum , CString
#stoptype

#starttype struct pkgbin
#stoptype

#opaque_t struct perpackagestate

#integral_t enum pkgwant
#integral_t enum pkgeflag
#integral_t enum pkgstatus
#integral_t enum pkgpriority

#opaque_t struct pkgiterator

#starttype struct pkginfo
#field set , Ptr <pkgset>
#field arch_next , Ptr <pkginfo>
#field want , <pkgwant>
#field eflag , <pkgeflag>
#field status , <pkgstatus>
#field priority , <pkgpriority>
#field otherpriority , CString
#field section , CString
#field configversion , <dpkg_version>
#field files , Ptr <filedetails>
#field installed , <pkgbin>
#field available , <pkgbin>
#field clientdata , Ptr <perpackagestate>
#stoptype

#starttype struct pkgset
#field next , Ptr <pkgset>
#field name , CString
#field pkg , <pkginfo>
#stoptype

#integral_t enum dpkg_msg_type

#starttype struct dpkg_error
#field type , <dpkg_msg_type>
#field str , CString
#stoptype

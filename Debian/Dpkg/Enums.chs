{-
 Enums.hs: Haskell bindings to libdpkg
   Copyright (C) 2011 Clint Adams

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
-}

{-# LANGUAGE ForeignFunctionInterface #-}
{-# LANGUAGE TypeSynonymInstances #-}

#include <dpkg/dpkg.h>
#include <dpkg/dpkg-db.h>
#include <dpkg/error.h>

{# context lib="dpkg" #}

module Debian.Dpkg.Enums (
    PkgWant(..)
  , PkgEflag(..)
  , PkgStatus(..)
  , PkgPriority(..)
) where

import Foreign.C.Types
import Foreign.C.String
import Foreign.Ptr
import Foreign.Storable

{#enum pkgwant as PkgWant {upcaseFirstLetter} deriving (Eq,Show) #}
{#enum pkgeflag as PkgEflag {upcaseFirstLetter} deriving (Eq,Show) #}
{#enum pkgstatus as PkgStatus {upcaseFirstLetter} deriving (Eq,Show) #}
{#enum pkgpriority as PkgPriority {upcaseFirstLetter} deriving (Eq,Show) #}

{#enum dpkg_msg_type as DpkgMsgType {upcaseFirstLetter} deriving (Eq,Show) #}
